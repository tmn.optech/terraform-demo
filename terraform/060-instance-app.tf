#### INSTANCE APP ####

# Create load balancer
resource "aws_elb" "app" {
  name    = "app-lb"
  subnets = [aws_subnet.app.id]

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 8084
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:8084/"
    interval            = 30
  }
  tags = {
    Name = "app-lb"
  }
}

# Create autoscaling group
resource "aws_autoscaling_group" "app" {
  name                      = "app-autoscaling-group"
  max_size                  = var.autoscaling_app["max_size"]
  min_size                  = var.autoscaling_app["min_size"]
  desired_capacity          = var.autoscaling_app["desired_capacity"]
  health_check_type         = "ELB"
  default_cooldown          = "30"
  health_check_grace_period = "120"
  force_delete              = true
  launch_configuration      = aws_launch_configuration.app.name
  load_balancers            = [aws_elb.app.name]
  termination_policies      = ["OldestLaunchConfiguration"]
  vpc_zone_identifier       = [aws_subnet.app.id]
}

# Configure instance launching configuration
resource "aws_launch_configuration" "app" {
  name_prefix   = "app"
  image_id      = data.aws_ami.centos.id
  instance_type = "t2.micro"
  key_name      = aws_key_pair.admin_key.key_name
  security_groups = [
    aws_security_group.administration.id,
    aws_security_group.app.id,
  ]
  user_data = file("scripts/mount-storage.sh")
}



