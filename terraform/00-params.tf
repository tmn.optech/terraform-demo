#### VPC Network
variable "vpc_cidr" {
  type    = string
  default = "192.168.0.0/16"
}

#### APP
variable "network_app" {
  type    = map(string)
  default = {
    subnet_name = "subnet_app"
    cidr        = "192.168.1.0/24"
  }
}

# Set number of instance
variable "autoscaling_app" {
  default = {
    desired_capacity = "2"
    max_size         = "5"
    min_size         = "2"
  }
}


#### DB PARAMS
variable "network_db" {
  type    = map(string)
  default = {
    subnet_name = "subnet_db"
    cidr        = "192.168.2.0/24"
  }
}

# Set number of instance
variable "autoscaling_db" {
  default = {
    desired_capacity = "2"
    max_size         = "5"
    min_size         = "1"
  }
}


