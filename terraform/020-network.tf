# Network configuration

# VPC creation
resource "aws_vpc" "terraform" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  tags = {
    Name = "vpc-app"
  }
}

# APP subnet configuration
resource "aws_subnet" "app" {
  vpc_id     = aws_vpc.terraform.id
  cidr_block = var.network_app["cidr"]
  tags = {
    Name = "subnet-app"
  }
  depends_on = [aws_internet_gateway.gw]
}

# DB subnet configuration
resource "aws_subnet" "db" {
  vpc_id     = aws_vpc.terraform.id
  cidr_block = var.network_db["cidr"]
  tags = {
    Name = "subnet-db"
  }
  depends_on = [aws_internet_gateway.gw]
}

# Gateway configuration
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.terraform.id
  tags = {
    Name = "internet-gateway"
  }
}

