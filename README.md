# DEMO работы с AWS в методологии IaC

Используемые инструменты в demo:

* [Terraform v0.12.19](https://releases.hashicorp.com/terraform/0.12.19/terraform_0.12.19_linux_amd64.zip)

* [Ansible v2.8.x](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

* [Dynamic inventory module](https://raw.githubusercontent.com/ansible/ansible/devel/contrib/inventory/ec2.py)

## Terraform

Необходимые инфраструктурные объекты:

-   1 VPC
-   2 Scaling group
-   3 Load balancer
-   4 Security group
-   5 Create ssh-key
-   6 EC2 instances app
-   7 EC2 instances db (mysql)
-   8 ELB (network volume)

### Создание инфраструктуры в AWS (Base level)

```
cd ./terraform
terraform init
terraform plan
terraform apply
```
> Требуется определить значение пременных в main.tf: AWS_ACCESS_KEY_ID; AWS_SECRET_ACCESS_KEY; AWS_REGION (us-east-1 us-east-2 ... etc)
> Требуется определить значение внешнего IP - адреса для доступа к ssh порту, так как остуствует в данной схеме bastion host.
030-security-group.tf:
```
# app port ssh
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["90.151.92.66/32"] ### заменить на свой IP, узнать можно перейти по адресу ping.eu
  }
...

# db port ssh
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["90.151.92.66/32"] ### заменить на свой IP, узнать можно перейти по адресу ping.eu
  }

```

### Удаление инфраструктуры

```
terraform destroy
```

## Ansible 

Необходимые условия: библиотека Boto

```
pip install boto
```

Добавить перменные в окружение 
```
export AWS_ACCESS_KEY_ID=
export AWS_SECRET_ACCESS_KEY=
export AWS_REGION=
```

Запуск playbooks

```
ansible-playbook -i ec2.py -u centos --become --become-user=centos playbooks/db.yml
ansible-playbook -i ec2.py -u centos --become --become-user=centos playbooks/app.yml
```
в playbooks/db.yml указываем в качестве хоста DB tag   - tag_Name_instance_db
в playbooks/app.yml указываем в качестве хоста APP tag  - tag_Name_instance_app_1


### Какой цели добились:

##### Гарантируется иденпотентность

##### Управление конфигурацией для операционных систем, промежуточного программного обеспечения и приложений:
Начальная конфигурация и дальнейшие изменения конфигурации полностью автоматизированы и управляются через центральную систему автоматической конфигурации

##### Документация
Документация охватывает все аспекты конфигурации и архитектуры, обновление документации является частью релиза, частично или полностью автоматизированного

##### Маштабирование
Приложение может автоматически масштабироваться на основе различных метрик

##### Автоматизация тестирования инфраструктуры:
Тестовые случаи созданы для проверки развернутой инфраструктуры на соответствие ожидаемым конфигурациям. Тестовые сценарии выполняются до развертывания

